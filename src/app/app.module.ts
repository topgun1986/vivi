import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { routing } from './app.routes';
import { IndustrySelectorComponent } from './pages/discovery-screen/industry-selector/industry-selector.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CheckboxModule, InputTextModule, RadioButtonModule } from 'primeng/primeng';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MobxAngularModule } from 'mobx-angular';
import { TextMaskModule } from 'angular2-text-mask';
import { HttpClientModule } from '@angular/common/http';
import { TabsModule } from 'ngx-bootstrap';
import { DiscoverScreenModule } from '@app/pages/discovery-screen/discover-screen.module';
import { ApplicationStore } from '@eventhorizon/stores/application.store';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    routing,
    DiscoverScreenModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CheckboxModule,
    TextMaskModule,
    RadioButtonModule,
    InputTextModule,
    CarouselModule.forRoot(),
    TabsModule.forRoot(),
    HttpClientModule,
    MobxAngularModule,
  ],
  providers: [
    ApplicationStore,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
