import { RouterModule, Routes } from '@angular/router';
import { DiscoveryScreenComponent } from './pages/discovery-screen/discovery-screen.component';

export const appRoutes: Routes = [
  { path: '', redirectTo: '/discovery', pathMatch: 'full' },
  { path: 'discovery', component: DiscoveryScreenComponent },
];
export const routing: any = RouterModule.forRoot(appRoutes, { useHash: true, enableTracing: true });
