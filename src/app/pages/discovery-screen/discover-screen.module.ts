import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { TextMaskModule } from 'angular2-text-mask';
import { MobxAngularModule } from 'mobx-angular';
import { EventhorizonModule } from '@eventhorizon/eventhorizon.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CarouselModule, TabsModule } from 'ngx-bootstrap';
import { CartComponent } from '@app/pages/discovery-screen/cart/cart.component';
import { IndustrySelectorComponent } from '@app/pages/discovery-screen/industry-selector/industry-selector.component';
import { PackageDetailsComponent } from '@app/pages/discovery-screen/package-details/package-details.component';
import { PackagesComponent } from '@app/pages/discovery-screen/packages/packages.component';
import { PaymentComponent } from '@app/pages/discovery-screen/payment/payment.component';
import { SolutionComponent } from '@app/pages/discovery-screen/solutions/solution/solution.component';
import { SolutionsComponent } from '@app/pages/discovery-screen/solutions/solutions.component';
import { DiscoveryScreenComponent } from '@app/pages/discovery-screen/discovery-screen.component';
import { AddressService } from '@eventhorizon/services/address.service';
import { BankService } from '@eventhorizon/services/bank.service';
import { TinService } from '@eventhorizon/services/tin.service';
import { CommonComponentsModule } from '@app/common-components/common-components.module';
import { BillingAddressComponent } from '@app/pages/discovery-screen/payment/billing-address/billing-address.component';
import { CreditCardComponent } from '@app/pages/discovery-screen/payment/credit-card/credit-card.component';
import { IndustryPanelComponent } from
    '@app/pages/discovery-screen/industry-selector/industry-panel/industry-panel.component';
import { ApplicationStore } from '@eventhorizon/stores/application.store';
import { CongratsComponent } from './congrats/congrats.component';
import { PackageComponent } from './package/package.component';
import { PricingComponent } from './pricing/pricing.component';
import { PricingPackageComponent } from './pricing/pricing-package/pricing-package.component';
import { IdentityVerificationComponent } from '@app/pages/discovery-screen/identity-verification/identity-verification.component';
import { StatusLineComponent } from '@app/pages/discovery-screen/identity-verification/status-line/status-line.component';

@NgModule({
  declarations: [
    CartComponent,
    CongratsComponent,
    CreditCardComponent,
    DiscoveryScreenComponent,
    IndustryPanelComponent,
    IndustrySelectorComponent,
    PackageDetailsComponent,
    PackagesComponent,
    PaymentComponent,
    SolutionComponent,
    SolutionsComponent,
    BillingAddressComponent,
    CongratsComponent,
    PackageComponent,
    PricingComponent,
    PricingPackageComponent,
    IdentityVerificationComponent,
    StatusLineComponent,
  ],
  imports: [
    EventhorizonModule,
    CommonComponentsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    EventhorizonModule,
    MobxAngularModule,
    TextMaskModule,
    HttpClientModule,
    CarouselModule.forRoot(),
    TabsModule.forRoot(),
  ],
  exports: [
  ],
  providers: [
    AddressService,
    ApplicationStore,
    BankService,
    TinService,
  ],
})
export class DiscoverScreenModule {}
