import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscoveryScreenComponent } from './discovery-screen.component';

describe('DiscoveryScreenComponent', () => {
  let component: DiscoveryScreenComponent;
  let fixture: ComponentFixture<DiscoveryScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscoveryScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscoveryScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
