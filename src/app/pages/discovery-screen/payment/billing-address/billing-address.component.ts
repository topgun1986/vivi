import { Component, OnInit } from '@angular/core';
import { ApplicationStore } from '@eventhorizon/stores/application.store';

@Component({
  selector: 'app-billing-address',
  templateUrl: './billing-address.component.html',
  styleUrls: ['./billing-address.component.scss'],
})
export class BillingAddressComponent implements OnInit {

  constructor(public store: ApplicationStore) { }

  ngOnInit() {
  }

}
