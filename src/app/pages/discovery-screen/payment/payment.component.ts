import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { CarouselSlide } from '@eventhorizon/components/carousel-slide';
import { CarouselComponent } from 'ngx-bootstrap';
import { ApplicationStore } from '@eventhorizon/stores/application.store';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss', '../cart/cart.component.scss'],
})
export class PaymentComponent extends CarouselSlide implements OnInit {

  constructor(public store: ApplicationStore,
              protected cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit() {
  }

}
