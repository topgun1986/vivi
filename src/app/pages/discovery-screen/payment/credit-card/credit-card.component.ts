import { Component, OnInit, Input } from '@angular/core';
import { masks } from '@eventhorizon/data/masks.data';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { randomComponentId } from '@eventhorizon/util/util';
import { MobxFormControl } from '@eventhorizon/components/mobx-form-control/mobx-form-control';
import { CommonValidator } from '@eventhorizon/validation/common.validator';
import { observable } from 'mobx';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss'],
})
export class CreditCardComponent implements OnInit {

  @Input()
  set readonly(value: boolean) {
    if (value === true) {
      this.fields.forEach(f => f.disable({ emitEvent: false }));
    } else {
      this.fields.forEach(f => f.enable({ emitEvent: false }));
    }
  }

  public form: FormGroup;

  public masks = masks;

  public id = randomComponentId();

  public ccName: MobxFormControl;
  public ccNumber: MobxFormControl;
  public ccMonth: FormControl;
  public ccYear: FormControl;
  public ccv: FormControl;

  constructor(protected fb: FormBuilder) {
  }

  ngOnInit() {
    this.buildForm();
  }

  public buildForm() {
    if (this.form) {
      return this.form;
    }

    this.ccName = new MobxFormControl('ccName', () => this.ccName, v => this.ccName = v, CommonValidator.personalName);
    this.ccNumber = new MobxFormControl('ccNumber', () => this.ccNumber, v => this.ccNumber = v);
    this.ccMonth = new FormControl('', Validators.required);
    this.ccYear = new FormControl('', Validators.required);
    this.ccv = new FormControl('', Validators.required);

    this.form = this.fb.group({
      ccName: this.ccName,
      ccNumber: this.ccNumber,
      ccMonth: this.ccMonth,
      ccYear: this.ccYear,
      ccv: this.ccv,
    });

    return this.form;
  }

  public validate() {
  }

  get fields() {
    if (!this.ccName) {
      return [];
    }
    return [this.ccName, this.ccNumber];
  }

}
