import { Component, Input, OnInit } from '@angular/core';
import { randomInt } from '@eventhorizon/util/util';

@Component({
  selector: 'app-status-line',
  templateUrl: './status-line.component.html',
  styleUrls: ['./status-line.component.scss'],
})
export class StatusLineComponent implements OnInit {

  @Input('text')
  public text: string;

  @Input('pass')
  public pass: boolean;

  @Input('details')
  public details: string[];

  public display1: boolean = false;
  public display2: boolean = false;
  public delay1: number = randomInt(1500, 2550);

  constructor() { }

  ngOnInit() {

  }

  public go() {
    this.display1 = true;
    setTimeout(() => {
      this.display2 = true;
    }, this.delay1);
  }

}
