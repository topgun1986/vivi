import { ChangeDetectorRef, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ApplicationStore } from '@eventhorizon/stores/application.store';
import { BaseOwnerComponent } from '@eventhorizon/components/base-owner/base-owner.component';
import { StatusLineComponent } from '@app/pages/discovery-screen/identity-verification/status-line/status-line.component';
import { CarouselSlide } from '@eventhorizon/components/carousel-slide';

@Component({
  selector: 'app-identity-verification',
  templateUrl: './identity-verification.component.html',
  styleUrls: ['./identity-verification.component.scss'],
})
export class IdentityVerificationComponent extends CarouselSlide implements OnInit {

  @ViewChildren(StatusLineComponent)
  protected statusLines: QueryList<StatusLineComponent>;

  public nameDetails: string[] = [];
  public namePass: boolean = false;
  public addressDetails: string[] = [];
  public addressPass: boolean = false;
  public emailDetails: string[] = [];
  public emailPass: boolean = false;
  public bankDetails: string[] = [];
  public bankPass: boolean = false;

  constructor(protected cd: ChangeDetectorRef,
              private store: ApplicationStore) {
    super(cd);
  }

  public ngOnInit() {}

  public onOpen() {
    const owner = this.store.owners[0];

    this.nameDetails = [`${owner.firstName} ${owner.lastName}`];
    this.namePass = true;

    this.addressDetails = [`${owner.address.address1} ${owner.address.address2}, ${owner.address.city} ${owner.address.state} ${owner.address.zip}`];
    this.addressPass = owner.firstName === 'Kyle';

    this.emailDetails = [owner.email];
    this.emailPass = true;

    this.bankPass = owner.firstName === 'Kyle';

    let timeout = 1000;
    this.statusLines.forEach((sl) => {
      setTimeout(() => { sl.go(); }, timeout);
      timeout += 150;
    });
  }
}
