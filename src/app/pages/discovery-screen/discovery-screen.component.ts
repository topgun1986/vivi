import { ApplicationStore } from '@eventhorizon/stores/application.store';
import { ActivatedRoute, Router } from '@angular/router';
import { messages } from '@eventhorizon/data/messages.data';
import { Component } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap';
import { BaseCarouselDriverComponent } from '@eventhorizon/components/base-carousel-driver/base-carousel-driver.component';

@Component({
  selector: 'app-discovery-screen',
  templateUrl: './discovery-screen.component.html',
  styleUrls: ['./discovery-screen.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 0, noPause: true, showIndicators: false } },
  ],
})
export class DiscoveryScreenComponent extends BaseCarouselDriverComponent {

  messages = messages;

  constructor(public store: ApplicationStore,
              protected route: ActivatedRoute,
              protected router: Router) {
    super(route, router);
  }

}

