import { Component, OnInit } from '@angular/core';
import { messages } from '@eventhorizon/data/messages.data';

@Component({
  selector: 'app-congrats',
  templateUrl: './congrats.component.html',
  styleUrls: ['./congrats.component.scss'],
})
export class CongratsComponent implements OnInit {

  public messages = messages.congratsSlide;

  constructor() { }

  ngOnInit() {
  }

}
