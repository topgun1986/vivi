import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CarouselSlide } from '@eventhorizon/components/carousel-slide';

@Component({
  selector: 'app-package-details',
  templateUrl: './package-details.component.html',
  styleUrls: ['./package-details.component.scss'],
})
export class PackageDetailsComponent extends CarouselSlide implements OnInit {

  constructor(protected cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit() {
  }

}
