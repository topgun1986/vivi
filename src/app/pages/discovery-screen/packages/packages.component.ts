import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CarouselSlide } from '@eventhorizon/components/carousel-slide';
import { Package } from '@eventhorizon/models/package.model';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss'],
})
export class PackagesComponent extends CarouselSlide implements OnInit {

  public package1 = <Package> {
    name: 'Clover Swift Package',
    image: './assets/images/Clover Flex.png',
    price: '$299.99',
    desc: 'A complete point-of-sale package perfect for restaurants, coffee shops, bars, and more.',
    bullets: ['Perfect for a small business',
      'Quick, easy to use.',
      'Secure.',
    ],
  };

  public package2 = <Package> {
    name: 'Clover Elite Package',
    image: './assets/images/Clover Station Package.png',
    price: '$499.99',
    desc: 'This complete point-of-sale solution is specifically designed to improve every aspect of the retail experience.',
    bullets: ['Perfect for restaurants.',
      'Easy replacement for old cash register systems.',
      'Secure.',
    ],
  };

  public package3 = <Package> {
    name: 'Clover Assist Package',
    image: './assets/images/Clover Family Package.jpg',
    price: '$699.99',
    desc: 'A complete point-of-scale package perfect for all beauty salons, health and wellness offices, gyms, and much more.',
    bullets: ['Complete solution.',
      'Manage schedules and appointments.',
      'Secure.',
    ],
  };

  constructor(protected cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit() {
  }

}
