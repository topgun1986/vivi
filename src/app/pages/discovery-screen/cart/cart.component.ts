import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormCarouselSlide } from '@eventhorizon/components/form-carousel-slide';
import { FormBuilder, FormControl } from '@angular/forms';
import { DialogService } from 'ng2-bootstrap-modal';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent extends FormCarouselSlide implements OnInit {

  public kitchenPrinter: FormControl;
  public paper: FormControl;
  public loginCard: FormControl;
  public weightScale: FormControl;

  constructor(protected cd: ChangeDetectorRef,
              protected ds: DialogService,
              private fb: FormBuilder) {
    super(cd, ds);
  }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm() {
    this.kitchenPrinter = new FormControl();
    this.paper = new FormControl();
    this.loginCard = new FormControl();
    this.weightScale = new FormControl();

    this.form = this.fb.group({
      kitchenPrinter: this.kitchenPrinter,
      paper: this.paper,
      loginCard: this.loginCard,
      weightScale: this.weightScale,
    });

  }

}
