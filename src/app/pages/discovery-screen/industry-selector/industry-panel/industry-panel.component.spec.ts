import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustryPanelComponent } from './industry-panel.component';

describe('IndustryPanelComponent', () => {
  let component: IndustryPanelComponent;
  let fixture: ComponentFixture<IndustryPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndustryPanelComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustryPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
