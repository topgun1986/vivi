import { Component, Input, OnInit } from '@angular/core';
import { ApplicationStore } from '@eventhorizon/stores/application.store';

@Component({
  selector: 'app-industry-panel',
  templateUrl: './industry-panel.component.html',
  styleUrls: ['./industry-panel.component.scss'],
})
export class IndustryPanelComponent implements OnInit {

  @Input() categories;

  @Input() offset: number = 0;

  constructor(private store: ApplicationStore) { }

  ngOnInit() {
  }

  public isSelected(index: number): boolean {
    return (this.store.industry === this.offset + index);
  }

  public select(index: number) {
    this.store.industry = this.offset + index;
  }

}
