import { ChangeDetectorRef, Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { CarouselComponent } from 'ngx-bootstrap';
import { CarouselSlide } from '@eventhorizon/components/carousel-slide';
import { categories } from '@eventhorizon/data/categories';

@Component({
  selector: 'app-industry-selector',
  templateUrl: './industry-selector.component.html',
  styleUrls: ['./industry-selector.component.scss'],
})
export class IndustrySelectorComponent extends CarouselSlide implements OnInit {

  categories1 = categories.slice(0, 5);
  categories2 = categories.slice(5, 11);

  @ViewChild(CarouselComponent)
  carousel: CarouselComponent;

  constructor(protected cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit() {
  }

  backSlide() {
    this.carousel.previousSlide();
  }

  nextSlide() {
    this.carousel.nextSlide();
  }

}
