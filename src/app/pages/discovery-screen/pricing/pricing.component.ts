import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { CarouselSlide } from '@eventhorizon/components/carousel-slide';
import { Pricing } from '@eventhorizon/models/pricing.model';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss'],
})
export class PricingComponent extends CarouselSlide implements OnInit {

  public package1 = <Pricing> {
    name: 'INTEGRATED',
    headline: 'A complete platform with simple, pay-as-you-go pricing',
    cost: '2.7%',
    cost2: ' + 5¢',
    costHeadline: 'per card present transaction',
    bullets: [
      'Integrated per-transaction',
      'No setup or monthly fees',
      'Price same across all cards',
    ],
    button: 'Select Plan',
  };

  public package2 = <Pricing> {
    name: 'INTEGRATED+',
    headline: 'Available for businesses with large payments volume',
    cost: '2.7%',
    cost2: ' + 5¢',
    costHeadline: 'per card present transaction',
    bullets: [
      'International cards +1%',
      'ACH direct debit 0.8%',
      'Global direct debit options',
    ],
    button: 'Select Plan',
  };

  public package3 = <Pricing> {
    name: 'CUSTOM',
    headline: 'Need a custom solution tailored for your unique business model?',
    cost: '-',
    cost2: '',
    costHeadline: 'Personalized your solution for:',
    bullets: [
      'Volume discounts',
      'Multi-product discounts',
      'Interchange pricing',
    ],
    button: 'Contact Us',
  };

  constructor(protected cd: ChangeDetectorRef) {
    super(cd);
  }

  ngOnInit() {
  }

}
