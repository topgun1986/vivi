import { Component, Input, OnInit } from '@angular/core';
import { Pricing } from '@eventhorizon/models/pricing.model';

@Component({
  selector: 'app-pricing-package',
  templateUrl: './pricing-package.component.html',
  styleUrls: ['./pricing-package.component.scss', '../pricing.component.scss'],
})
export class PricingPackageComponent implements OnInit {

  @Input()
  public package: Pricing;

  public selected: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  public select() {
    this.selected = !this.selected;
  }

}
