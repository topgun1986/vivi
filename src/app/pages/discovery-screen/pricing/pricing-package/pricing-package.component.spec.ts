import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricingPackageComponent } from './pricing-package.component';

describe('PricingPackageComponent', () => {
  let component: PricingPackageComponent;
  let fixture: ComponentFixture<PricingPackageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricingPackageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricingPackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
