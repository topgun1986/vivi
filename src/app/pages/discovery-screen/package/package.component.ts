import { Component, Input, OnInit } from '@angular/core';
import { Package } from '@eventhorizon/models/package.model';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss', '../packages/packages.component.scss'],
})
export class PackageComponent implements OnInit {

  @Input()
  public package: Package;

  constructor() { }

  ngOnInit() {
  }

}
