import { Component } from '@angular/core';
import { ApplicationStore } from '@eventhorizon/stores/application.store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Bank of Miramar';

  public constructor(private store: ApplicationStore) {
    window['__store'] = store;
  }
}
