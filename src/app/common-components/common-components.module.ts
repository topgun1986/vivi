import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { TextMaskModule } from 'angular2-text-mask';
import { MobxAngularModule } from 'mobx-angular';
import { EventhorizonModule } from '@eventhorizon/eventhorizon.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CarouselModule, TabsModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    EventhorizonModule,
    MobxAngularModule,
    TextMaskModule,
    HttpClientModule,
    CarouselModule.forRoot(),
    TabsModule.forRoot(),
  ],
  exports: [
  ],
  providers: [
  ],
})
export class CommonComponentsModule {}
