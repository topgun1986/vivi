export const errors = {
  'address1-invalid': 'Enter a valid address name',
  'address1-required': 'Required',

  'address2-invalid': 'Enter a valid address2 name',

  'account-number-invalid': 'Enter a valid bank account number',
  'account-number-required': 'Required',

  'bank-name-invalid': 'Enter a valid bank name',
  'bank-name-required': 'Required',

  'city-invalid': 'Enter a valid city name',
  'city-required': 'Required',

  'date-invalid': 'Enter a valid date (mm/dd/yyyy)',
  'date-required': 'Required',

  'dba-name-invalid': 'DBA Name must be 2-24 letters',
  'dba-name-required': 'Required',

  'dob-invalid': 'Enter a valid date of birth (mm/dd/yyyy)',
  'dob-required': 'Required',

  'email-invalid': 'Enter a valid email address',
  'email-required': 'Required',

  'legal-name-invalid': 'Legal Name must be 2-24 letters',
  'legal-name-required': 'Required',

  'ownership-invalid': 'Enter a number between 1 and 100',
  'ownership-required': 'Required',

  'personal-name-invalid': 'Enter a valid name',
  'personal-name-required': 'Required',

  'phone-number-invalid': 'Enter a valid 7 digit US phone number',
  'phone-number-required': 'Required',

  'required': 'Required',

  'routing-invalid': 'Enter a valid routing number',
  'routing-required': 'Required',

  'state-invalid': 'Enter a valid US state',
  'state-required': 'Required',

  ssnInvalid: 'Enter a valid US SSN number',
  'ssn-match': 'SSN do not match',
  ssnRequired: 'Required',

  'tax-name-invalid': 'Tax Name must be 2-24 letters',
  'tax-name-required': 'Required',

  'total-ownership-invalid': 'Total ownership must be between 0 and 100',
  'total-ownership-required': 'Required',

  'tin-invalid': 'Enter a valid tax id number',
  'tin-required': 'Required',
  'tin-match': 'Tax Id numbers do not match',

  'website-invalid': 'Enter a valid website address',

  'zip-invalid': 'Enter a valid US zip code',
  'zip-required': 'Required',

};
