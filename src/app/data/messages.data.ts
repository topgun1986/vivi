export const messages = {

  address: {

  },

  bankScreen: {

  },

  businessInfoScreen: {

  },

  ownerScreen: {

    additionalOwnersLabel: 'Are there any other owners with 25% or more equity?',
    additionalOwnersYes: 'Yes',
    additionalOwnersNo: 'No',

    ownershipLabel: 'Ownership',
  },

};
