#!/bin/bash

ng build
pyenv shell 3.6.6
python ../../CoreScripts/DeployToAzureBlobStorage.py topgun-ui ./dist/miramar/
